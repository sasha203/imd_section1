package com.sa.sa_section1;



import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.MyViewHolder> {

    private LayoutInflater mLayoutInflater;
    private Context ctx;
    private ArrayList<BluetoothDevice> mDevices;
   private int  mViewResourceId;



    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView deviceName;
        public TextView deviceAddress;

        public MyViewHolder(View v) {
            super(v);
            deviceName = (TextView)itemView.findViewById(R.id.tvDeviceName);
            deviceAddress = (TextView)itemView.findViewById(R.id.tvDeviceAddress);
        }

    }

    public DeviceListAdapter(Context c, ArrayList<BluetoothDevice> devices, int tvResourceId){
        //super(context, tvResourceId,devices);
        this.ctx = c;
        this.mDevices = devices;
        mLayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //mLayoutInflater = LayoutInflater.from(ctx);
        mViewResourceId = tvResourceId;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mLayoutInflater.inflate(R.layout.device_adapter_view, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    //DeviceListAdapter.MyViewHolder holder, int position

        BluetoothDevice device = mDevices.get(position);
        if (holder != null) {

            if (holder.deviceName != null) {
                holder.deviceName.setText(device.getName());
            }
            if (holder.deviceAddress != null) {
                holder.deviceAddress.setText(device.getAddress());
            }
        }

    }




    /*
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(mViewResourceId, null);

        BluetoothDevice device = mDevices.get(position);

        if (device != null) {
            TextView deviceName = (TextView) convertView.findViewById(R.id.tvDeviceName);
            TextView deviceAddress = (TextView) convertView.findViewById(R.id.tvDeviceAddress);

            if (deviceName != null) {
                deviceName.setText(device.getName());
            }
            if (deviceAddress != null) {
                deviceAddress.setText(device.getAddress());
            }
        }

        return convertView;
    }

    */






    @Override
    public int getItemCount() {
        return mDevices.size();
    }

}

